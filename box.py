import cv2
import numpy as np

def processImage(Image):
    #Kernel size
    ksize = 3
    gray = cv2.cvtColor(img,cv2.COLOR_RGB2GRAY)
    # gray = cv2.cvtColor(img,cv2.CV_LOAD_IMAGE_GRAYSCALE)

    # ret,thresh = cv2.threshold(gray, 200, 255,cv2.THRESH_BINARY)
    # cimg = cv2.cvtColor(thresh,cv2.COLOR_GRAY2BGR)
    
    # Apply GuassianBlur to reduce noise.
    blur = cv2.GaussianBlur(gray,(5,5),0) 
    # blur = cv2.medianBlur(gray,5)
    
    # Adaptive Guassian Threshold
    thresh = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,11,3.5)
    # thresh = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 31, 2)


    # global thresholding
    ret1,th1 = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)
 
    # Otsu's thresholding
    ret2,th2 = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    # Otsu's thresholding after Gaussian filtering
    ret3,th3 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    # Morphological Transformations
    # kernel = np.ones((ksize,ksize))/(ksize*ksize)
    kernel = np.ones((3,3),np.uint8)
    # kernel = np.ones((2.6,2.7),np.uint8)
    final = cv2.erode(thresh,kernel,iterations = 1)
    final = cv2.dilate(final,kernel,iterations = 1)

    ret, thresh1 = cv2.threshold(blur,175,255,cv2.THRESH_TOZERO)
    canny = cv2.Canny(thresh,100,200)
    return final

img = cv2.imread("box.jpg")
scale_percent = 50 # percent of original size
width = int(img.shape[1] * scale_percent / 100)
height = int(img.shape[0] * scale_percent / 100)
dim = (width, height)
img = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)

# Create mask
height,width = img.shape
mask = np.zeros((height,width), np.uint8)

processed_img = processImage(img)
original_img = img.copy()
contour_img = processed_img.copy()

# cv2.imshow("Image",contour_img)

# _, contours, _= cv2.findContours(contour_img,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
# contours, heirarchy = cv2.findContours(contour_img,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)
# contours = sorted(contours, key=cv2.contourArea, reverse=True)[:10]

# circles = cv2.HoughCircles(contour_img, cv2.HOUGH_GRADIENT, 22, minDist=1, maxRadius=40)
# circles = cv2.HoughCircles(contour_img, cv2.HOUGH_GRADIENT, 1, 20, param1=30, param2=15, minRadius=0, maxRadius=0)
circles = cv2.HoughCircles(contour_img, cv2.HOUGH_GRADIENT, 1, 20, param1=10, param2=40, maxRadius=70)
# circles = cv2.HoughCircles(image=contour_img, method=cv2.HOUGH_GRADIENT, dp=0.9, minDist=80, param1=110, param2=39, maxRadius=70)
# circles = cv2.HoughCircles(contour_img, cv2.HOUGH_GRADIENT, 1, 100, param1=100, param2=100, minRadius=0, maxRadius=300)
# circles = cv2.HoughCircles(contour_img, cv2.HOUGH_GRADIENT, 1.5, 10)

circles = np.uint16(np.around(circles))

# if circles is not None:
# 	circles = np.round(circles[0, :]).astype("int")
# 	for (x, y, r) in circles:
# 		cv2.circle(img, (x, y), r, (0, 255, 0), 1)
# 		cv2.rectangle(img, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)

if circles is not None:
    for i in circles[0,:]:
        cv2.circle(img,(i[0],i[1]),i[2],(0,255,0),2)
        cv2.circle(img,(i[0],i[1]),2,(0,0,255),3)
        # cv2.putText(img,str(len(circles[0])-1),(10,50),cv2.FONT_HERSHEY_COMPLEX,2,(255,0,0),2)
        print (str(len(circles[0])-1))

# for contour in contours:
#     p = cv2.arcLength(contour,True)
#     approx = cv2.approxPolyDP(contour,0.02*p,True)
    # print (len(approx))
    # if len(approx) == 4:
    #     x,y,w,h = cv2.boundingRect(contour)
    #     final_img1 = original_img[y:y+h,x:x+w]
    #     cv2.imshow("Detected : ",final_img1)
    #     # cv2.rectangle(img,(y,y+h),(x,x+w),(0,255,0),2)
    #     cv2.drawContours(img, contours, -1, (0,255,255), 3)

    # elif len(approx) == 8:
    #     area = cv2.contourArea(contour)
    #     (cx, cy), radius = cv2.minEnclosingCircle(contour)
    #     # print (radius)
    #     center = (int(cx),int(cy))
    #     radius = int(radius)
    #     circleArea = radius * radius * np.pi
    #     # print (circleArea)
    #     # print (area)
    #     # if circleArea == area:
    #     x,y,w,h = cv2.boundingRect(contour)
    #     final_img2 = original_img[y:y+h,x:x+w]
    #     # cv2.imshow("Detected : ",final_img2)
    #     cv2.circle(img,center,radius,(0,255,0),2)
    #     # cv2.drawContours(img, contours, -1, (0,255,255), 3)
    #     # cv2.drawContours(img, contours, -1, (220, 152, 91), -1)

cv2.imshow("Image",img)
cv2.waitKey(0)
cv2.destroyAllWindows()